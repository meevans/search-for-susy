# Search for SUSY
## Jupyter notebook with steps to search for Supersymmetry (SUSY) yourself!
------

## Get Started (online)
Click on this link ---> [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.cern.ch%2Fmeevans%2Fsearch-for-susy/master)

Click on Search-for-SUSY.ipynb


## Get Started (on your own laptop)
On a terminal type "git clone https://gitlab.cern.ch/meevans/search-for-susy.git"

Open a [Jupyter notebook](https://jupyter.org) using your favourite [Python](https://www.python.org) (3.6 or above) environment (mine is [Anaconda](https://www.anaconda.com/distribution/))

Click on search-for-susy.ipynb (either the original one you downloaded or your version that you downloaded)


![SUSY_WZ Feynman diagram](SUSY_WZ_feynman.png)
